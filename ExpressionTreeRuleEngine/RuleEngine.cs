﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ExpressionTreeRuleEngine.Rules;

namespace ExpressionTreeRuleEngine
{
    public class RuleEngine
    {
        public static RuleEngine Init()
        {
            return new RuleEngine();
        }


        //partner, <domain, rules>
        //multi tenancy, key is the partner.
        //RuleBag is the bag of rules grouped by domain
        private readonly Dictionary<String, RulesBag> _ruleBags; 

        protected RuleEngine()
        {
            _ruleBags = new Dictionary<string, RulesBag> {{"ARG", new RulesBag()}};
        }


        public void CreateRule<T>(string domain, RuleType ruleType, T param) 
        {
            var rule = RuleFactory.CreateRule(ruleType, param);
            _ruleBags["ARG"].Add(domain, rule);
        }

        public bool Run(string domain, string input)
        {
            List<Func<string, bool>> rules;
            if (_ruleBags["ARG"].Rules.TryGetValue(domain, out rules))
            {
               return  rules.Any(rule => rule.Invoke(input));
            };
            //free country
            return true;

        }
    }
}
