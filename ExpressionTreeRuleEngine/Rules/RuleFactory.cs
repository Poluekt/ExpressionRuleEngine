﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExpressionTreeRuleEngine.Rules
{
    public class RuleFactory
    {
        public static Func<string, bool> CreateRule<T>(RuleType ruleType, T param)
        {
            Func<string, bool> func = null;

            switch (ruleType)
            {
                case RuleType.RegexMatch:
                    func = RegexRule(param as string);
                    break;
                case RuleType.TypeValidation:
                    func = TypeValidation(param as Type);
                    break;
            }
            return func;
        }

        #region deleagates
        private static Func<string, bool> TypeValidation(Type param)
        {
            Expression<Func<string, bool>> expr = text => IsValid(param, text);
            return expr.Compile();
        }


        private static bool IsValid(Type type, string text)
        {
            switch (type.Name)
            {
                case "Int32":
                    int t;
                    return int.TryParse(text, out t);
            }
            return false;

        }


        private static Func<string, bool> RegexRule(string param)
        {
            Expression<Func<string, bool>> expr = input => new Regex(param).IsMatch(input);
            return expr.Compile();
        }

        #endregion
    }
}
