﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTreeRuleEngine.Rules
{
   public  class RulesBag 
    {
       public Dictionary<string, List<Func<string, bool>>> Rules = new Dictionary<string, List<Func<string, bool>>>();
        

       public RulesBag()
       {
          // Rules = new Dictionary<string, Func<Object, bool>>();
       }


       public void Add(string domain, Func<string, bool> rule)
       {
           if (Rules.ContainsKey(domain))
           {
               Rules[domain].Add(rule);

           }
           else
           {
               Rules.Add(domain, new List<Func<string, bool>>
               {
                   rule
               });
           }
       }
    }

}
