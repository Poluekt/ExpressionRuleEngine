﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpressionTreeRuleEngine;
using ExpressionTreeRuleEngine.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RuleEngineTests
{
    [TestClass]
    public class BarringNumbersTests : BaseTestClass
    {
        [TestMethod]
        public void TestBarringNumbers_RuleEngine_NumberBlocked()
        {
            //Arrange
<<<<<<< HEAD
            var phoneNumbers = new[]
=======
           var  phoneNumbers = new[]
>>>>>>> 9a327bdf0cba3e9ec1528d77e1e7ab470d88532e
            {
                "0610" + Random.Next(1000000, 9000000),
                "0600"+ Random.Next(1000000, 9000000)
            };

<<<<<<< HEAD
            var engine = RuleEngine.Init();
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, @"0610[0-9]{7}");
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, @"0600[0-9]{7}");

            //ACT
            var answers = phoneNumbers.Select(phone =>
             {
                 var isBarred = engine.Run("DialCodeVerification", phone);
                 return isBarred;
             }).ToList();

            //ASSERT
            Assert.IsTrue(answers.All(x => x));

        }


        [TestMethod]
        public void TestBarringNumbers_RuleEngine_LegitNumber()
        {
            //Arrange
            var phone = "972" + Random.Next(1000000, 9000000);

            var engine = RuleEngine.Init();
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, @"0610[0-9]{7}");
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, @"0600[0-9]{7}");

            //ACT
            var isBarred = engine.Run("DialCodeVerification", phone);

            //ASSERT
            Assert.IsFalse(isBarred);
=======

            var engine = RuleEngine.Init();
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, @"0610[0-9]{7}");
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, @"0600[0-9]{7}");
            



            var ans = engine.Run("DialCodeVerification", phoneNumbers[0]);
            Assert.IsTrue(ans);
            //ACT
           var answers = phoneNumbers.Select(phone =>
            {
                var isBarred = engine.Run("DialCodeVerification", phone);


                return isBarred;
            }).ToList();

            //ASSERT
           
     //      Assert.IsTrue(isAllBarred);

>>>>>>> 9a327bdf0cba3e9ec1528d77e1e7ab470d88532e
        }

    }
}
