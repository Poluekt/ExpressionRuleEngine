﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RuleEngineTests
{
    [TestClass]
    public abstract class BaseTestClass
    {
        protected static Random Random;

        [AssemblyInitialize]
        public static void Init(TestContext context)
        {
            Random = new Random();
        }


        protected static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }
    }
}
