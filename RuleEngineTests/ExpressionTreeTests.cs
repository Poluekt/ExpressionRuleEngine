﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using ExpressionTreeRuleEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RuleEngineTests
{
    [TestClass]
    public class ExpressionTreeTests : BaseTestClass
    {
        private const string Pattern = @"^[0-9]*$";


        [TestMethod]
        public void TestPowerSingleExpression()
        {
            int n = Random.Next(10);
            var expected = (int)Math.Pow(n, 2);
             
            Expression<Func<int, int>> powerLambda = num => num*num;
            var result = powerLambda.Compile().DynamicInvoke(n);
            
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void TestAdditionSingleExpression()
        {
            int a = Random.Next(10);
            int b = Random.Next(10);
            var expected = a + b;

            Expression<Func<int, int, int>> addLambda = (x, y) => x + y;
            
            Delegate deleg = addLambda.Compile();
            var result = deleg.DynamicInvoke(a, b);

            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void TestRegexSingleExpression_MatchNumber_Success()
        {
            Expression<Func<string, string, bool>> regexLambda = (tavnit, text) => new Regex(tavnit).IsMatch(text);
            string inputStr = Random.Next(100000).ToString();

            Delegate deleg = regexLambda.Compile();
            var result = (bool) deleg.DynamicInvoke(Pattern, inputStr);

            Assert.IsTrue(result);
        }


        [TestMethod]
        public void TestRegexMultiExpression_MatchIP_Success()
        {
            // Arrange
            var regPatterns = new []
            {
              new Regex(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"),      //IP pattern
              new Regex  (@"^[0-9]$"),                              //single digit
              new Regex (@"^[A-Z]*$")                              //Capital letters 
            };

            string inputIp = String.Format("{0}.{1}.{2}.{3}", Random.Next(255), Random.Next(255), Random.Next(255),
                Random.Next(255));


            int parsedNumber;
            //rules
            Expression<Func<Regex, string, bool>> regexLambda = (tavnit, text) => tavnit.IsMatch(text);
            Expression<Func<string, bool>> parseLambda = text => Int32.TryParse(text, out parsedNumber);

            
            //Act
            var result = regPatterns.Any(pattern => (bool)regexLambda.Compile().DynamicInvoke(pattern, inputIp));
            
            
            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void TestRegexMultiExpression_MatchLowerCaseString_Failure()
        {
            // Arrange
            var regPatterns = new[]
            {
              new Regex(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"),      //IP pattern
              new Regex  (@"^[0-9]$"),                              //single digit
              new Regex (@"^[A-Z]*$")                              //Capital letters 
            };

            string lowerInput = RandomString(7).ToLower();
            string upperInput = lowerInput.ToUpper();


            Expression<Func<Regex, string, bool>> regexLambda = (tavnit, text) => tavnit.IsMatch(text);

            //Act
            var resultLower = regPatterns.Any(pattern => (bool)regexLambda.Compile().DynamicInvoke(pattern, lowerInput));
            var resultUpper = regPatterns.Any(pattern => (bool)regexLambda.Compile().DynamicInvoke(pattern, upperInput));


            



            //Assert
            Assert.IsFalse(resultLower);
            Assert.IsTrue(resultUpper);
        }




    }
}
