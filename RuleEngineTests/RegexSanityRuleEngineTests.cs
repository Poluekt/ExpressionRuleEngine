﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using ExpressionTreeRuleEngine;
using ExpressionTreeRuleEngine.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RuleEngineTests
{
    [TestClass]
    public class RegexSanityRuleEngineTests : BaseTestClass
    {
   
        private static RuleEngine _ruleEngine;
        private const string Pattern = @"^[0-9]*$";

        [ClassInitialize]
        public static void Init(TestContext context)
        {
 

            _ruleEngine = RuleEngine.Init();
            _ruleEngine.CreateRule("UnitTestDomain", RuleType.RegexMatch, Pattern);
        }


        [TestMethod]
        public void TestRegex_SimpleNumberRegex_Success()
        {
            string pattern = @"^[0-9]*$";
            string input = Random.Next(100000).ToString();

            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            Assert.IsTrue(rgx.IsMatch(input));
        }

        [TestMethod]
        public void TestRegex_SimpleNumberRegex_Failed()
        {


            string pattern = @"^[0-9]*$";
            string input = Guid.NewGuid().ToString();

            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            Assert.IsFalse(rgx.IsMatch(input));
        }



        [TestMethod]
        public void TestRuleEngine_SimpleNumberRegex_Success()
        {

            //Arrange
            string pattern = @"^[0-9]*$";
            string input = Random.Next(100000).ToString();


            var engine = RuleEngine.Init();
            engine.CreateRule("DialCodeVerification", RuleType.RegexMatch, pattern);
            engine.CreateRule("DialCodeVerification", RuleType.TypeValidation, typeof (Int32));
           

            //ACT
           var foundMatch =  engine.Run("DialCodeVerification", input);

            //ASSERT
           Assert.IsTrue(foundMatch);
        }   
    }
}
