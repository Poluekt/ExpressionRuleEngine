﻿using System;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RuleEngineTests
{
    [TestClass]
    public class RegexSanityTests : BaseTestClass
    {
        private const string Pattern = @"^[0-9]*$";

        [TestMethod]
        public void TestRegex_SimpleNumberRegex_Success()
        {
            string input = Random.Next(100000).ToString();

            var rgx = new Regex(Pattern, RegexOptions.IgnoreCase);
            Assert.IsTrue(rgx.IsMatch(input));
        }

        [TestMethod]
        public void TestRegex_SimpleNumberRegex_Failed()
        {
            string input = Guid.NewGuid().ToString();

            var rgx = new Regex(Pattern, RegexOptions.IgnoreCase);
            Assert.IsFalse(rgx.IsMatch(input));
        }


    }
}
